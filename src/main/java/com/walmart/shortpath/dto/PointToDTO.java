package com.walmart.shortpath.dto;

public class PointToDTO {
	public final PointFromDTO target;
	public final double weight;
	public PointToDTO(PointFromDTO argTarget, double argWeight){ 
		target = argTarget; 
		weight = argWeight; 
	}
}
