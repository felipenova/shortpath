package com.walmart.shortpath.dto;

import java.util.ArrayList;
import java.util.List;

public class PointFromDTO implements Comparable<PointFromDTO>{
	public String name;
	public List<PointToDTO> adjacencies = new ArrayList<>();
	public double minDistance = Double.POSITIVE_INFINITY;
	public PointFromDTO previous;
	
	public String getName() {
		return name;
	}
	public PointFromDTO(String argName) { 
		name = argName; 
	}
	public String toString() { 
		return name; 
	}
	public int compareTo(PointFromDTO other){
		return Double.compare(minDistance, other.minDistance);
	}
}
