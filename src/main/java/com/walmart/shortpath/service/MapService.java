package com.walmart.shortpath.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.walmart.shortpath.dao.MapDAO;
import com.walmart.shortpath.dto.PointFromDTO;
import com.walmart.shortpath.dto.PointToDTO;
import com.walmart.shortpath.model.Map;
import com.walmart.shortpath.model.Route;

/**
 * Class service responsible for application logic and call the DAO for database manipulation.
 * @author Felipe Nova
 *
 */
@Transactional(readOnly = true)
public class MapService {

	private static final Logger logger = LoggerFactory.getLogger(MapService.class);

	@Autowired
	private MapDAO mapDao; 


	/**
	 * Responsible method to call the DAO method that makes the inclusion in the database .
	 * @param em
	 * @return Map
	 * @throws Exception
	 */
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = { Exception.class },readOnly = false)
	public Map insert(Map em) throws Exception{
		try {
			Map map =  mapDao.insert(em);
			return map;
		} catch(Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	/**
	 * Method responsible for compute all distances for later use.
	 * @param source
	 */
	public static void computePaths(PointFromDTO source){
		source.minDistance = 0.;
		PriorityQueue<PointFromDTO> pointFromQueue = new PriorityQueue<PointFromDTO>();
		pointFromQueue.add(source);

		while (!pointFromQueue.isEmpty()) {
			PointFromDTO u = pointFromQueue.poll();

			for (PointToDTO e : u.adjacencies)
			{
				PointFromDTO pf = e.target;
				double weight = e.weight;
				double distanceThroughU = u.minDistance + weight;
				if (distanceThroughU < pf.minDistance) {
					pointFromQueue.remove(pf);

					pf.minDistance = distanceThroughU ;
					pf.previous = u;
					pointFromQueue.add(pf);
				}
			}
		}
	}

	/**
	 * Method responsible for verficar which the fastest route.
	 * @param target
	 * @return List<PointFromDTO>
	 */
	public static List<PointFromDTO> getShortestPathTo(PointFromDTO target){
		List<PointFromDTO> path = new ArrayList<PointFromDTO>();
		for (PointFromDTO pointFrom = target; pointFrom != null; pointFrom = pointFrom.previous)
			path.add(pointFrom);

		Collections.reverse(path);
		return path;
	}

	/**
	 * Method responsible for making all logic and calculate the cost .
	 * @param mapName
	 * @param origin
	 * @param destiny
	 * @param autonomy
	 * @param literFuelValue
	 * @return String
	 * @throws Exception
	 */
	public String calculate(String mapName, String origin, String destiny,Integer autonomy, Double literFuelValue) throws Exception{
		Map map = mapDao.getMapByName(mapName);
		Set<String> uniqueOrigins = new HashSet<String>();
		for(Route r:map.getRoutes()){
			uniqueOrigins.add(r.getOrigin());
			uniqueOrigins.add(r.getDestiny());
		}
		List<PointFromDTO> pfs = new ArrayList<>();
		for(String uo:uniqueOrigins){
			pfs.add(new PointFromDTO(uo));
		}
		for(PointFromDTO pf:pfs){
			for(Route r:map.getRoutes()){
				if(pf.getName().equalsIgnoreCase(r.getOrigin())){
					pf.adjacencies.add(new PointToDTO(getPointFromDTOByName(pfs,r.getDestiny()), r.getDistance()));
				}
			}
		}

		PointFromDTO from = getPointFromDTOByName(pfs,origin);
		if(from == null){
			throw new IllegalArgumentException("Não foi encontrada a origem "+origin);
		}
		computePaths(from); 
		PointFromDTO to = getPointFromDTOByName(pfs,destiny);
		if(to == null){
			throw new IllegalArgumentException("Não foi encontrado o destino "+destiny);
		}
		double minDist = to.minDistance;
		List<PointFromDTO> path = getShortestPathTo(to);
		if(path.size() <= 1 ){
			throw new IllegalArgumentException("Não existe rota com origem: "+origin+" e destino "+destiny);
		}
		Double cost = (minDist/autonomy)*literFuelValue;


		return "A menor rota é "+path+" com custo de "+ cost;
	}

	/**
	 * Method responsible for taking PointFromDTO from a list by name.
	 * @param pfs
	 * @param name
	 * @return PointFromDTO
	 */
	public PointFromDTO getPointFromDTOByName(List<PointFromDTO> pfs, String name){
		for(PointFromDTO pf:pfs){
			if(pf.getName().equalsIgnoreCase(name)){
				return pf;
			}
		}

		return null;
	}


}
