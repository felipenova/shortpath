package com.walmart.shortpath.dao;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.walmart.shortpath.model.Map;
import com.walmart.shortpath.model.Route;

/**
 * Class responsible for database manipulation.
 * @author Felipe Nova
 *
 */
@Repository
public class MapJpaDAO implements MapDAO {
	@PersistenceContext(unitName="shortPath_DataSource", name="shortPath_DataSource")
	private EntityManager manager;

	/**
	 * Method responsible for direct insertion in the database .
	 * @param map
	 * @return Map
	 */
	@Transactional
	public Map insert(Map map) throws Exception {
		for(Route route:map.getRoutes()){
			route.setMap(map);
		}
		manager.persist(map);
		manager.flush();
		return map;
	}

	/**
	 * Method in charge of getting the map in the database by name.
	 * @param mapName
	 * @return Map
	 */
	@Override
	public Map getMapByName(String mapName) throws Exception {
		try{
		TypedQuery<Map> q = manager.createNamedQuery("search.name", Map.class);
		q.setParameter("mapName", mapName.toUpperCase());
		return q.getSingleResult();
		}catch(NoResultException e){
			throw new NoResultException("Não foi encontrado um mapa com o nome de "+mapName);
		}
	}

}
