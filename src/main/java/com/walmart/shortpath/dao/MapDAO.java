package com.walmart.shortpath.dao;

import com.walmart.shortpath.model.Map;

public interface MapDAO {
	public Map insert(Map map) throws Exception;
	public Map getMapByName(String mapName) throws Exception;
}
