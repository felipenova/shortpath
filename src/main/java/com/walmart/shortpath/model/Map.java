package com.walmart.shortpath.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name="Map") 
@NamedQueries(value={
		@NamedQuery(name="search.name", 
				query="select m from Map m where upper(m.name) = :mapName")/*,
				@NamedQuery(name="search.subject", 
				query="select e from ExchangeMessage e where e.subject like :subject"),
				@NamedQuery(name="search.date.hour", 
				query="select e from ExchangeMessage e where e.insertDate >= :dateHourStart and e.insertDate <= :dateHourEnd")*/

})
public class Map implements Serializable{

	private static final long serialVersionUID = 2298496846865861222L;


	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@Size(max=100)
	@Column(name="name",nullable=false,length=100,unique=true)
	private String name;
	
	@OneToMany(mappedBy = "map", targetEntity = Route.class, fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private List<Route> routes;
	
	public Map(){}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Route> getRoutes() {
		return routes;
	}

	public void setRoutes(List<Route> routes) {
		this.routes = routes;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Map other = (Map) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

}
