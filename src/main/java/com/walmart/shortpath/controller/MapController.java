package com.walmart.shortpath.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Produces;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.walmart.shortpath.model.Map;
import com.walmart.shortpath.service.MapService;

/**
 * 
 * @author Felipe Nova
 * Class responsible for exposing the rest layer
 *
 */
@Controller
@RequestMapping("/rest")
public class MapController {

	private static final Logger logger = LoggerFactory.getLogger(MapController.class);

	@Autowired
	private MapService mapService;

	/**
	 * Method responsible for receiving the request and call the method that will make the integration of data.
	 * @param map
	 * @param request
	 * @return String
	 */
	@RequestMapping(value = "/insertMap", method = RequestMethod.POST)
	@Produces(MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> insertMap(@RequestBody Map map,HttpServletRequest request){	
		try{
			mapService.insert(map);
			return new ResponseEntity<String>("OK", HttpStatus.OK);
		}catch(Exception e){
			logger.error(e.getMessage());
			return new ResponseEntity<String>("Ocorreu um erro na gravação do mapa!",HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Method responsible for receiving the request and call the method that will do the calculation.
	 * @param request
	 * @param resp
	 * @return String
	 * 
	 */
	@RequestMapping(value = "/calculate", method = RequestMethod.GET,produces = "text/plain;charset=UTF-8")
	public @ResponseBody ResponseEntity<String> calculate(HttpServletRequest request, HttpServletResponse resp) {
		String response = "";
		String mapName 		= request.getParameter("mapName");
		String origin 		= request.getParameter("origin");
		String destiny 		= request.getParameter("destiny");
		Integer autonomy 		= 0;
		try{
			autonomy = Integer.parseInt(request.getParameter("autonomy"));
		}catch(NumberFormatException e){
			return new ResponseEntity<String>("A autonomia deve ser um número inteiro",HttpStatus.INTERNAL_SERVER_ERROR);
		}

		Double literFuelValue 		= 0.0;
		try{
			literFuelValue = Double.parseDouble(request.getParameter("literFuelValue").replace(",", "."));
		}catch(NumberFormatException e){
			return new ResponseEntity<String>("O valor do litro deve ser um número decimal",HttpStatus.INTERNAL_SERVER_ERROR);
		}
		try{
			response = mapService.calculate(mapName, origin, destiny, autonomy, literFuelValue);
		}catch(Exception e){
			e.printStackTrace();
			return new ResponseEntity<String>(e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<String>(response, HttpStatus.OK);
	}


}
