# Projeto Short Path #


### Requisitos ###

* Java 7
* MySql
* Maven
* WildFly 10

### Passo a passo ###

* Criar o Banco de dados

Obs. Os scripts estão na pasta docs -> database_scripts
```
#!command

create database shortpath;
mysql -u username -p shortpath < ShortPath_Map.sql
mysql -u username -p shortpath < ShortPath_Route.sql
```
Configurar no servidor WildFly 10 o módulo do Mysql caso ele não exista, para isso deve-se baixar o jar do mysql-connector no link [Mysql Connector](https://dev.mysql.com/downloads/connector/j/)

Considerando que você já tem o wildfily instalado. Acesse a pasta onde está o servidor “wildfly\modules\system\layers\base\com”.
Crie a pasta “mysql” e dentro dela a pasta “main”. Nesta pasta crie o um arquivo chamado “module.xml”. Seu conteúdo será igual ao seguinte:


```
#!xml

<?xml version="1.0" encoding="UTF-8"?>  
<module xmlns="urn:jboss:module:1.0" name="com.mysql">
 <resources>  
 <resource-root path="mysql-connector-java-VERSION.jar"/>  
 </resources>  
 <dependencies>  
 <module name="javax.api"/>  
 <module name="javax.transaction.api"/>  
 </dependencies>  
</module>

```
Alterando para a versão baixada, testada na versão 5.1.38

Acesse novamente a pasta onde foi instalado o Servidor Wildfly “wildfly\standalone\configuration” e abra o arquivo: “standalone.xml”
Procure a ocorrência da tag que indica onde devemos adicionar os datasources ”urn:jboss:domain:datasources:2.0".


```
#!xml
<datasource jta="false" jndi-name="java:/jboss/jdbc/shortPath_DataSource" pool-name="shortPath_DataSource" enabled="true" use-ccm="false">
                    <connection-url>jdbc:mysql://localhost:3306/shortpath</connection-url>
                    <driver-class>com.mysql.jdbc.Driver</driver-class>
                    <driver>com.mysql</driver>
                    <security>
                        <user-name>(USUARIO DO BANCO)</user-name>
                        <password>(SENHA DO BANCO)</password>
                    </security>
                    <validation>
                        <validate-on-match>false</validate-on-match>
                        <background-validation>false</background-validation>
                    </validation>
                    <statement>
                        <share-prepared-statements>false</share-prepared-statements>
                    </statement>
</datasource>

```
Onde USUARIO DO BANCO e SENHA DO BANCO deve ser alterado para o banco criado.

Para gerar o aqruivo .war deve-se entrar na pasta do projeto e executar:

```
#!maven

mvn clean install
```

Depois deve-se entrar na pasta target dentro do projeto, copiar o arquivo shortpath-1.0.0.war e colocar dentro da pasta deployments do WildFly 10.

Depois deve-se subir o servidor entrando na pasta bin do WildFly e executando o seguinte comando:

Windows:
```
#!command

standalone.bat
```
Linux:
```
#!command

./standalone.sh
```
Depois disso a aplicação está no ar.

Serviços:


```
#!http

POST http://localhost:8080/shortpath-1.0.0/rest/insertMap
GET http://localhost:8080/shortpath-1.0.0/rest/calculate
```

Exemplo de parâmetro do serviço "calculate"

```
#!http

?mapName=mapa1&origin=A&destiny=F&autonomy=10&literFuelValue=2.5

```

Exemplo de request para o serviço "insertMap"


```
#!json

{
   "name":"mapa1",
   "routes":[
      {
         "origin":"A",
         "destiny":"B",
         "distance":5
      },
      {
         "origin":"A",
         "destiny":"C",
         "distance":10
      },
      {
         "origin":"B",
         "destiny":"D",
         "distance":3
      },
      {
         "origin":"B",
         "destiny":"G",
         "distance":10
      },
      {
         "origin":"C",
         "destiny":"D",
         "distance":10
      },
      {
         "origin":"C",
         "destiny":"E",
         "distance":10
      },
      {
         "origin":"D",
         "destiny":"F",
         "distance":10
      },
      {
         "origin":"D",
         "destiny":"E",
         "distance":5
      },
      {
         "origin":"E",
         "destiny":"F",
         "distance":4
      },
      {
         "origin":"E",
         "destiny":"H",
         "distance":1
      },
      {
         "origin":"G",
         "destiny":"F",
         "distancia":10
      },
      {
         "origin":"H",
         "destiny":"F",
         "distance":1
      }
   ]
}
```